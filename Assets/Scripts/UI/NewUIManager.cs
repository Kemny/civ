using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
public class NewUIManager : MonoBehaviour
{
    [SerializeField] Chunk chunk; 
    Label tileName = null;
    Index selectedIndex;
    
    // Start is called before the first frame update
    void Start()
    {
        UIDocument doc = GetComponent<UIDocument>();
        tileName = doc.rootVisualElement.Q<Label>("TileName");

        doc.rootVisualElement.Q<Button>("Forrest").clicked += delegate {chunk.SetTileType(selectedIndex, TileType.Forrest);};
        doc.rootVisualElement.Q<Button>("Desert").clicked += delegate {chunk.SetTileType(selectedIndex, TileType.Desert);};
        doc.rootVisualElement.Q<Button>("Mountain").clicked += delegate {chunk.SetTileType(selectedIndex, TileType.Mountain);};
    }

    public void SelectTile(Index index)
    {
        selectedIndex = index;
        tileName.text = index.ToString();
    }
}
