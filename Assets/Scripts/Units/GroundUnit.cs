using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundUnit : Unit
{
    public override void StartMoving(List<Index> path)
    {
        if (path.Count < 1 || !CanMove(path[path.Count-1]))
        {
            Debug.LogError("Invalid path");
            return;
        }
        unitMover.StartMoving(path);
    }

    public override List<Index> GetPathTo(Index targetTile)
    {
        Chunk.GetChunk(new Index(0, 0)).Astar(GetIndex(), targetTile, out List<Index> path);

        if (path.Count > 1)
        {
            return path;
        }
        else
        {
            return null;
        }
    }
}
