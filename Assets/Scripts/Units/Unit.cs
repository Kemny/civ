using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//useless comment
public class Unit : MonoBehaviour
{
  protected UnitMover unitMover = null;

  void Start()
  {
    unitMover = GetComponent<UnitMover>();
  }

  public virtual void StartMoving(List<Index> path)
  {
    Debug.LogError("Trying to move with base unit");
  }

  public Index GetIndex()
  {
    return unitMover.GetTileIndex();
  }

  public bool CanMove(Index targetTile)
  {
    return GetPathTo(targetTile) != null;
  }

  public virtual List<Index> GetPathTo(Index targetTile)
  {
    Debug.LogError("Trying to move with base unit");
    return null;
  }
}

