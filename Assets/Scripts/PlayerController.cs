using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//useless comment
public class PlayerController : MonoBehaviour
{
    [SerializeField] NewUIManager uIManager;
    [SerializeField] Chunk chunkSelected;
    Unit unit;
    
    void Start()
    {
        
    }
    
    //raycasts ant tells if hits
    void Update()
    {
        chunkSelected.UnHighlightTiles();
        
        if ((Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit)))
        {
            HandleRaycastHit(hit);
        }
    }
    
    //if hits returns what it hited
    void HandleRaycastHit(RaycastHit hit)
    {
        if (!hit.collider || !hit.collider.gameObject)
        {
            return;
        }

        ObjectTag foundTag = hit.collider.gameObject.GetComponent<ObjectTag>();
        if (!foundTag)
        {
            return;
        }

        switch (foundTag.GetObjectTag())
        {
            case ObjectTag.EObjectType.Tile:
                HandleTileHit(foundTag);
                break;
                    
            case ObjectTag.EObjectType.Player:
                break;
                    
            case ObjectTag.EObjectType.Unit:
                HandleUnitHit(foundTag);
                break;
                    
            default:
                Debug.LogError("Missing enum implementation");
                break;
        }
    }
    
    //if there is mouse action then ui will be shown or unit starts moving
    void HandleTileHit(ObjectTag tag)
    {
        Tile foundTile = tag.GetObjectTransform().GetComponent<Tile>();

        if (!foundTile)
        {
            return;
        }
        
        if (Input.GetMouseButtonDown(1))
        {
            uIManager.SelectTile(foundTile.index);
        }

        if (unit != null)
        {
            if (unit.CanMove(foundTile.index))
            {
                List<Index> path = unit.GetPathTo(foundTile.index);

                // highlight path
                foreach (var tile in path)
                {
                    chunkSelected.SetHighlighted(tile, true);
                }
                
                if (Input.GetMouseButtonDown(0))
                {
                    unit.StartMoving(path);
                }  
            }
        }
    }
    void HandleUnitHit(ObjectTag tag)
    {
        if (Input.GetMouseButtonDown(0))
        {
            //if unit is clicked his object is saved
            unit = tag.GetObjectTransform().gameObject.GetComponent<Unit>();
        }
    }
}
