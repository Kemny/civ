﻿using UnityEngine;

// Vypnutí varovaní číslo CS0414
#pragma warning disable CS0414

namespace Examples
{
    public class FunctionExamples : MonoBehaviour
    {
        // Tato funkce se vyvolá pomocí "BasicFunction()". Zavolá všechen kód uprostřed těchto závorek
        void BasicFunction()
        {
            
        }

        // Tato funkce se vyvolá pomocí "ReturningFunction()". Zavolá všechen kód uprostřed těchto závorek ale než se ukončí, musí vrátit int protože má int jako svůj return type. Tato proměná se po ukončení bude "rovnat" proměné kterou vrátí
        int ReturningFunction()
        {
            if (enabled)
            {
                return 0; // Tato funkce vrátí 0 a bude se "rovnat 0" (int x = ReturningFunction(); x == 0 )
            }
            else
            {
                int a = 5;
                return a; // Tato funkce vrátí 0 a bude se "rovnat 5" (int x = ReturningFunction(); x == 5 )
            }
            // Zde už žádný return být nemusí, protože funkce se buď ukončí v if, nebo v else. Pokud by byla možnost že by se kód mohl dostat sem, musel by být zde také return
        }

        // Tato funkce se vyvolá pomocí "ReturningFunction(x, y)" Tato funkce potřebuje nějáké 2 int proměné pro své spuštění. Tyto 2 inty se zkopírují do této funkce pod jménem "a" a "b"
        void ParameterFunction(int a, int b)
        {
            Debug.Log(a + b); // Tato funkce například může vytisknout součet dvou čísel
        }

        // Příklad použití těchto konceptů pro zjednoduššení práce 
        int ReturningParameterFunction(int a, int b)
        {
            if (a > b)
            {
                return a - b;
            }

            // Místo toho, abych napsal else, dal jsem return na konec funkce
            return a + b;
        }

        int nonstaticVariable = 5;
        static int staticVariable = 5;
        // Statické funkce nepotřebují být volané na žádném objektu, ale také nemají přistup k nestatickým proměným vytvořených v této třidě. (FunctionExamples.Return5())
        public static int Return5()
        {
            // return nonstaticVariable <- error
            return staticVariable;
        }
    }
}

// Opětné zapnutí varovaní číslo CS0414
#pragma warning restore CS0414