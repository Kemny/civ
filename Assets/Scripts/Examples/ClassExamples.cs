﻿// Oznámení které namespace nechceme manuálně vypysovat (UnityEngine.GameObject -> GameObject)
using UnityEngine;

// namespace = "složka" ve které jsou classy
namespace Examples
{
    // "Recept" pro náš objekt pod jménem ClassExamples
    public class ClassExamples : MonoBehaviour // ClassExamples dědí od MonoBehaviour, stává se z této classy komponent který může být přidán k objektům
    { // <- Otevírání těla třídy
        
    } // <- Zavírání těla třídy
}