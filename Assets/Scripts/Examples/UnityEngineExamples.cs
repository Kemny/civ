﻿using UnityEngine;


namespace Examples
{
    public class UnityEngineExamples : MonoBehaviour 
    {
        // Start je vyvolán Unity enginem za nás. Zavolá se jakmile se spawne objekt s tímto componentem
        void Start()
        {
            
        }

        // Update je vyvolán Unity enginem za nás. Zavolá se jednou za každý frame
        void Update()
        {
            
        }

        // Rostoucí list funkcí v MonoBehaviour.
        void Functions()
        {
            // GetComponent<**Typ Componentu**>() - Získá component daného typu na stejném objektu jako tento komponent, null pokud komponent neexistuje
            
        }

        // Rostoucí list properties v MonoBehaviour. Property je proměná s logikou při používání, je doporučeno omezit používání
        void Properties()
        {
            /*
            gameObject  - rovná se hernímu objektu na který je tento komponent přidán
            transform   - rovná se transform komponentu na stejném objektu jako tento komponent
            enabled     - jakmile je enabled true, unity pro nás volá funkce jako update a start. Pro výkon je dobré vypínat componenty které mají update, ale právě ho nepotřebují
            */
        }
    }
}
