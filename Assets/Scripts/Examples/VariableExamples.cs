﻿using System;
using UnityEngine;

// Vypnutí varovaní číslo CS0414
#pragma warning disable CS0414

namespace Examples
{
    public class VariableExamples : MonoBehaviour
    {
        private int a;                  // Soukromá proměná. Můžeme jí používat pouze uvnitř těla třídy
        protected int b;                // Chráněná proměná. Můžeme jí používat pouze uvnitř těla třídy a v tělech třídách které z této třídy dědí
        public int c;                   // Veřejná proměná. Kdokoliv jí může číst a měnit jakmile mluví k tomuto objektu. Ztrácíme nad ní jakoukoliv kontrolu. Je doporučeno používat GET property, nebo funkci která proměnou vrací pro udržení kontroly
        [SerializeField] private int z; // SerializeField proměné jsou private, ale vidět v editoru

        public static int d; // Statická proměná. Statické věci nepatří konktrétnímu objektu, ale celému typu třídy. Jsou sdílené mezi všemi instancemi, a pro jejich používání není ani instance potřeba. (VariableExamples.d)

        void SomeFunction()
        {
            // Proměné existují pouze v těle závorek ve kterých byly vytvořeny.

            int e = 5;
            
            {
                int f = 10;

                int g = e + f; // 15. 'e' i 'f' nejsou mimo své závorky
            }

            //int h = g + e; //<- Toto je error, protože promená 'g' byla zničena jakmile opustila své závorky 
        }

        // Value types <- Rychle vytvořitelné proměné, které se vždy kopírují mezi funkcemi
        int i = 10;             // int je celé číslo mezi -2,147,483,648 a 2,147,483,647
        float j = 0.1f;         // float je číslo s desetinou tečkou. Teoreticky nemá limit, ale při velikých hodnotách může začít zaokrouhlovat až příliš (5 milionů + 1 == 5 milionů)
        bool l = true;          // bool je true/false. Realná velikost není 1 bit, ale minimálně 4 
        char m = 'E';           // char je jedno písmeno o velikosti encodování textu. (většinou 8)
        
        // Reference types <- Pomalu vytořitelné proměné, které se nikdy nekopírují mezi funkcemi. Mouhou být null
        string k = "Some Text"; // string je List několika characterů
        VariableExamples n;     // Uložení reference na jakoukoliv classu kterou uživatel vytvořil. V tomto případě 'VariableExamples'
    }
}

// Opětné zapnutí varovaní číslo CS0414
#pragma warning restore CS0414