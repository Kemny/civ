﻿using System;
using UnityEngine;

namespace Examples
{
    public class test1
    {
        private void A()
        {
            Debug.Log("Hello world.");
        }

        void B(int count)
        {
            for (int i = 0; i < 5; i++)
            {
                i = C(i, 5);
                if (i == 2)
                {
                    break;
                }
                else
                {
                    return;
                }
            }
        }

        int C(int x, int y)
        {
            return x * y;
        }

        void D()
        {
            int z = 5;
            z -= 2;
            test2 x;
            x = new test2();
            x.v = 6;
            x.SetW(x.v);
            Debug.Log(x.GetW());
        }
    }
    public class test2
    {
        public int v;
        private int w;

        public void SetW(int w2)
        {
            w = w2;
        }

        public int GetW()
        {
            return w;
        }
    }
}