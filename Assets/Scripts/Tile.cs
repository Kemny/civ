using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//nodes have states 
public enum NodeState
{
    Empty,
    Building,
    Unit
}

//tiles have types
public enum TileType
{
    Forrest,
    Desert,
    Mountain
}

public struct Index
{
    public int x;
    public int y;

    public Index(int X, int Y)
    {
        x = X;
        y = Y;
    }

    public Vector2 ToVector2()
    {
        return new Vector2(x, y);
    }
    public static Index operator +(Index a, Index b)
    {
        return new Index(a.x + b.x, a.y + b.y);
    }
    public static Index operator -(Index a, Index b)
    {
        return new Index(a.x - b.x, a.y - b.y);
    }
    public static bool operator ==(Index a, Index b)
    {
        return a.x == b.x && a.y == b.y;
    }
    public static bool operator !=(Index a, Index b)
    {
        return a.x != b.x || a.y != b.y;
    }

    public override string ToString()
    {
        return base.ToString() + " " + x.ToString() + " " + y.ToString();
    }
}

//there are a lot of variables
public class Tile : MonoBehaviour
{
    public MeshRenderer render;
    
    public Index index;

    public float estimatedCost;
    public int totalCost;
    public Tile previousTile;

    public TileType currentType;
    public NodeState state = NodeState.Unit;

    public int GetTileCost()
    {
        switch (currentType)
        {
            case TileType.Forrest: return 0;
                
            case TileType.Desert: return 2;
                
            case TileType.Mountain: return 99;
                
            default: 
                Debug.LogError("Unknown Tile cost");
                break;
        }

        return 99;
    }
} 
