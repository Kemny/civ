﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//useless comment
[DisallowMultipleComponent]
public class Movement : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed = 2;
    [SerializeField] float jumpSpeed = 2;
    
    void Start()
    {
        Debug.Log("Hello World!");
    }

    void Update()
    {
        //helps move unit in direction
        Vector3 addPosition = Vector3.zero;
        
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            addPosition += Vector3.forward * Time.deltaTime * speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            addPosition += Vector3.back * Time.deltaTime * speed;
        }
        
        if (Mathf.Approximately(rb.velocity.y, 0) && Input.GetKeyDown(KeyCode.Space))
        {
            addPosition += Vector3.up * Time.deltaTime * jumpSpeed;
        }
        
        rb.AddForce(addPosition);
    }
}
