using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//useless comment
public class ObjectTag : MonoBehaviour
{
    
    //vianble object tipes
   public enum EObjectType
    {
        Tile,
        Player,
        Unit
    }

   [SerializeField] Transform targetTransform;
   [SerializeField] EObjectType type;

   //takes wanted unit to targeted position
   void Awake()
   {
       if (!targetTransform)
       {
           targetTransform = transform;
       }
   }

   public EObjectType GetObjectTag()
    {
        return type; 
    }

   public Transform GetObjectTransform()
    {
        return targetTransform; 
    }
}
