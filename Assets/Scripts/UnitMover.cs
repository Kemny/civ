using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//useless comment
public class UnitMover : MonoBehaviour
{
    Vector3 startPosition;

    Vector3 endPosition;

    private float alpha;

    Index tileIndex;

    List<Index> path;

    int listTarget;

    public Index GetTileIndex()
    {
        return tileIndex;
    }

    void Awake()
    {
        enabled = false;
    }

    //starts unit mover
    void Update()
    {
        UpdateUnitMovement();
    }

    //lerps unit to endposition
    void UpdateUnitMovement()
    {
        alpha += Time.deltaTime;

        float x, y, z;

        x = Mathf.Lerp(startPosition.x, endPosition.x, alpha);
        y = Mathf.Lerp(startPosition.y, endPosition.y, alpha);
        z = Mathf.Lerp(startPosition.z, endPosition.z, alpha);

        Vector3 newPosition = new Vector3(x, y, z);

        transform.position = newPosition;

        if (alpha >= 1)
        {
            OnTargetReached();
        }
    }

    void OnTargetReached()
    {
        tileIndex = path[listTarget];
        
        listTarget++;
        
        if (path.Count <= listTarget)
        {
            enabled = false;
        }
        else
        {
            alpha = 0;
            Vector3 targetPosition = new Vector3(path[listTarget].x, 0, path[listTarget].y);
            endPosition = targetPosition;

            startPosition = transform.position;  
        }
    }

    //moves unit over the path
    public void StartMoving(List<Index> path)
    {
        if (enabled)
        {
            return;
        }
        
        tileIndex = path[0];
        
        this.path = path;
        listTarget = 1;
        
        alpha = 0;
        Vector3 targetPosition = new Vector3(path[listTarget].x, 0, path[listTarget].y);
        endPosition = targetPosition;

        startPosition = transform.position;
        enabled = true;
    }
}
