using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//useless comment
public class Looker : MonoBehaviour
{
    //normalises mouse position on screen
    [SerializeField] float lookRange = 7;
    [SerializeField] Transform debugObject;
    void Update()
    {
        Vector3 mousePosNormalized;
        mousePosNormalized.x = 0;
        mousePosNormalized.z = (Input.mousePosition.x / Screen.width - 0.5f) * lookRange * 2;
        mousePosNormalized.y = (Input.mousePosition.y / Screen.height - 0.5f) * lookRange * 2;

        mousePosNormalized += transform.position;

        debugObject.transform.position = mousePosNormalized;
        
        transform.LookAt(mousePosNormalized);
    }
}
