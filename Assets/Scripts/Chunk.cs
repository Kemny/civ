using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//useless comment
public class Chunk : MonoBehaviour
{
    //default variables
    [SerializeField] Tile tilePrefab;
                  
    Dictionary<Index, Tile> tiles;

    [SerializeField] Vector2Int chunkSize;

    [SerializeField] private TileSettings tileSettings;

    Index index = new Index(0, 0);

    static Dictionary<Index, Chunk> chunks = new();

    //after start spawn random colored tiles
    void Start()
    {
        tiles = new Dictionary<Index, Tile>();
        
        for (int x = 0; x < chunkSize.x; x++)
        {
            for (int y = 0; y < chunkSize.y; y++)
            {
                Tile spawnedTile = Instantiate(tilePrefab, new Vector3(x, 0, y), Quaternion.identity, transform);
                spawnedTile.index = new Index(x, y);
                tiles.Add(spawnedTile.index, spawnedTile);

                int random = Random.Range(0, 3);
                TileType type = (TileType) random;
                SetTileType(new Index(x, y), type);
            }
        }
        
        chunks.Add(index, this);
    }
     public void UnHighlightTiles()
    {
        foreach (var tile in tiles)
        {
            SetHighlighted(tile.Key, false);
        }
    }

    //diferent tiles have diferent types and colors
    public void SetTileType(Index index, TileType tileType)
    {
        tiles[index].currentType = tileType;

        switch (tileType)
        {
            case TileType.Desert:
                tiles[index].render.material.SetColor(tileSettings.colorParameterName, tileSettings.desertColor);
                break;
        
            case TileType.Forrest:
                tiles[index].render.material.SetColor(tileSettings.colorParameterName, tileSettings.forrestColor);
                break;
        
            case TileType.Mountain:
                tiles[index].render.material.SetColor(tileSettings.colorParameterName, tileSettings.mountainColor);
                break;
        }
    }

    public void SetHighlighted(Index index, bool highlighted)
    {
        float fHighlighted;
        if (highlighted == true)
        {
            fHighlighted = 1;
        }
        else
        {
            fHighlighted = 0;
        }
        tiles[index].render.material.SetFloat(tileSettings.colorHighlightedParameterName, fHighlighted);
    }

    //get time returns tile type
    public TileType GetTileType(Index index)
    {
        return tiles[index].currentType;
    }
    
    //every tile knows their neighbours 
    List<Tile> GetTileNeighbors(Tile tile)
    {
        List<Tile> retVal = new List<Tile>();
        
        if (tiles.ContainsKey(tile.index + new Index(1,0)))
        {
            retVal.Add(tiles[tile.index + new Index(1, 0)]);
        }
        
        if (tiles.ContainsKey(tile.index + new Index(-1,0)))
        {
            retVal.Add(tiles[tile.index + new Index(-1,0)]);
        }
        
        if (tiles.ContainsKey(tile.index + new Index(0,1)))
        {
            retVal.Add(tiles[tile.index + new Index(0,1)]);
        }
        
        if (tiles.ContainsKey(tile.index + new Index(0,-1)))
        {
            retVal.Add(tiles[tile.index + new Index(0,-1)]);
        }
        
        return retVal;
    }

    public bool GetStraitPathTo(Index start, Index end, out List<Index> foundPath)
    {
        if (!tiles.ContainsKey(start))
        {
            Debug.LogError("Start Tile Is Incorrect");
            foundPath = new List<Index>();
            return false;
        }
    
        if (!tiles.ContainsKey(end))
        {
            Debug.LogError("End Tile Is Incorrect");
            foundPath = new List<Index>();
            return false;
        }

        /*Vector2 direction = (end - start).ToVector2().normalized;*/

        Index pathLenght = end - start;
        int sign = -1;
        if (pathLenght.x > 0)
        {
            sign = 1;
        }
        
        for (int x = 0; x == pathLenght.x; x = x+sign)
        {
            foundPath.Add(new Index(x + start.x, start.y));
        }
    }
    
    //astart gets start index and end index and returns path
    public bool Astar(Index start, Index end, out List<Index> foundPath)
    {
        if (!tiles.ContainsKey(start))
        {
            Debug.LogError("Start Tile Is Incorrect");
            foundPath = new List<Index>();
            return false;
        }
    
        if (!tiles.ContainsKey(end))
        {
            Debug.LogError("End Tile Is Incorrect");
            foundPath = new List<Index>();
            return false;
        }
        
        foreach (var tile in tiles)
        {
            tile.Value.estimatedCost = 0;
            tile.Value.totalCost = 0;
            tile.Value.previousTile = null;
        }
        
        var openSet = new List<Index>();
        var closedSet = new List<Index>();
        
        openSet.Add(start);
        
        while(openSet.Count > 0)
        {
            Tile currentTile = tiles[openSet[0]];
            
            // What is the preferred tile
            for (int i = 0; i < openSet.Count; i++)
            {
                Tile possibleTile = tiles[openSet[i]];
                
                if (possibleTile.totalCost < currentTile.totalCost)
                {
                    currentTile = possibleTile;
                }
                else if (possibleTile.totalCost == currentTile.totalCost)
                {
                    if (possibleTile.estimatedCost < currentTile.estimatedCost)
                    {
                        currentTile = possibleTile;
                    }
                }
            }
            
            if (currentTile.index == end)
            {
                foundPath = new List<Index>();
                Tile temp = currentTile;

                foundPath.Add(temp.index);

                while (temp.previousTile != null)
                {
                    foundPath.Add(temp.previousTile.index);
                    temp = temp.previousTile;
                }
                foundPath.RemoveAt(foundPath.Count - 1);
                foundPath.Reverse();
                return true;
            }
            
            openSet.Remove(currentTile.index);
            closedSet.Add(currentTile.index);
            
            // What tiles to check
            foreach (var neighbor in GetTileNeighbors(currentTile))
            {
                if (neighbor.currentType == TileType.Mountain)
                {
                    continue;
                }
                
                if (closedSet.Contains(neighbor.index))
                {
                    continue;
                }

                bool newPath = false;
                int totalCost = currentTile.totalCost + neighbor.GetTileCost();
                
                if (openSet.Contains(neighbor.index))
                {
                    if (totalCost < neighbor.totalCost)
                    {
                        neighbor.totalCost = totalCost;
                        newPath = true;
                    }
                }
                else
                {
                    neighbor.totalCost = totalCost;
                    openSet.Add(neighbor.index);
                    newPath = true;
                }

                if (newPath)
                {
                    int x = System.Math.Abs(currentTile.index.x - neighbor.index.x);
                    int y = System.Math.Abs(currentTile.index.y - neighbor.index.y);
                    neighbor.estimatedCost = x + y;
                    neighbor.totalCost = totalCost;
                    neighbor.previousTile = currentTile;
                }
            }
        }

        Debug.Log("Path not found");
        foundPath = new List<Index>();
        return false;
    }

    static public Chunk GetChunk(Index index)
    {
        return chunks[index];
    }
}