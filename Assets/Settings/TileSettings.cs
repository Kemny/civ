﻿using UnityEngine;


[CreateAssetMenu(fileName = "TileSettings", menuName = "TileSettings", order = 0)]
public class TileSettings : ScriptableObject
{
    [SerializeField] public Color desertColor;
    [SerializeField] public Color forrestColor;
    [SerializeField] public Color mountainColor;

    [SerializeField] public string colorParameterName = "BaseColor";
    [SerializeField] public string colorHighlightedParameterName = "bHighlighted";
}
